## Taxonomy Server README
### Table of Contents
* [Viewing all vocabularies]
* [Retrieve all terms in a vocabulary]

### Viewing all vocabularies

Requests can be made for all vocabularies at the following endpoint:

```
/taxonomy_server_sample_service/taxonomy_server_vocabulary
```

Sample Response:

```json
{
  vocabulary_0: {
    vocabulary_id: "999",
    vocabulary_name: "Taxonomy Server Sample Vocabulary",
    vocabulary_machine_name: "taxonomy_server_sample_vocabulary",
    vocabulary_description: "Sample vocabulary to be used with the taxonomy_server_sample_vocabulary feature."
  }
}
```

The vocabulary id filter will return all taxonomy terms in that vocabulary:

```
/taxonomy_server_sample_service/taxonomy_server_vocabulary/999
```

Sample Response:

```json
{
  vocabulary_id: "999",
  vocabulary_name: "Taxonomy Server Sample Vocabulary",
  vocabulary_machine_name: "taxonomy_server_sample_vocabulary",
  vocabulary_description: "Sample vocabulary to be used with the taxonomy_server_sample_vocabulary feature.",
  terms: {
    term_0: {
    term_name: "Test term",
    term_description: "a term",
    term_weight: "0",
    term_uuid: "84ed6b9c-81cd-466c-ba21-6c3b3ccd55d4",
    term_synonyms: null,
    term_parents: {
    term_parents_parent_uuid_0: [ ]
  },
  term_relations: [ ]
  },
  term_1: {
    term_name: "another term",
    term_description: "",
    term_weight: "0",
    term_uuid: "ad39ecc9-b5c0-4f73-a6a2-82c1902aff8a",
    term_synonyms: null,
    term_parents: {
    term_parents_parent_uuid_0: [ ]
  },
  term_relations: [ ]
    }
  }
}
```
