<?php
/**
 * @file
 * taxonomy_server.resource.inc
 */

/**
 * Implements hook_services_resources().
 *
 * Defines function signatures for resources available to services.
 */
function taxonomy_server_services_resources() {
  return array(
    'taxonomy_server_vocabulary' => array(
      'index' => array(
        'help' => 'Instructions for calling this resource\'s retrieve operation.',
        'file' => array('file' => 'inc', 'module' => 'taxonomy_server'),
        'callback' => '_taxonomy_server_vocabulary_index',
        'access callback' => '_taxonomy_server_access',
        'access arguments' => array('index'),
      ),
      'retrieve' => array(
        'help' => 'Returns revision data for a provided vocabulary\'s taxonomy terms.',
        'file' => array('file' => 'inc', 'module' => 'taxonomy_server'),
        'callback' => '_taxonomy_server_vocabulary_retrieve',
        'access callback' => '_taxonomy_server_access',
        'access arguments' => array('view'),
        'access arguments append' => TRUE,
        'args' => array(
          array(
            'name' => 'vid',
            'type' => 'int',
            'description' => 'The vocabulary vid',
            'source' => array('path' => '0'),
            'optional' => TRUE,
          ),
        ),
      ),
    ),
  );
}

/**
 * Access callback for the taxonomy server vocabulary resource.
 *
 * @param string $op
 *   The operation that's going to be performed.
 *
 * @return bool
 *   Whether access is given or not.
 */
function _taxonomy_server_access($op) {
  $access = TRUE;
  switch ($op) {
    case 'index':
      break;

    case 'term':
      break;

    case 'view':
      break;

  }
  return $access;
}

/**
 * Callback for the taxonomy server vocabulary resource.
 *
 * Returns information on what the api can return.
 *
 * Index operation.
 *
 * @return array
 *   An array.
 */
function _taxonomy_server_vocabulary_index() {
  drupal_add_http_header('Status', '200 OK');

  // @TODO: How do we want to restrict access to some vocabularies?
  // This will expose all vocabularies.
  $return = array();
  $vocabularies = taxonomy_get_vocabularies();
  $i = 0;
  foreach ($vocabularies as $vocabulary) {
    $v['vocabulary_id'] = $vocabulary->vid;
    $v['vocabulary_name'] = $vocabulary->name;
    $v['vocabulary_machine_name'] = $vocabulary->machine_name;
    $v['vocabulary_description'] = $vocabulary->description;
    $return['vocabulary_' . $i++] = $v;
  }

  return (object) $return;
}

/**
 * Callback for the taxonomy server vocabulary resource.
 *
 * Retrieve operation.
 *
 * @param int $v_vid
 *   The term's vocabulary id.
 *
 * @return array
 *   An array.
 */
function _taxonomy_server_vocabulary_retrieve($v_vid) {
  $return = array();
  $vocabulary = taxonomy_server_get_vocabulary($v_vid);
  if (!$vocabulary) {
    return array(
      'message' => 'vid is not valid',
    );
  }
  $terms = taxonomy_server_get_all_terms($v_vid);

  // Remove drupalisms from names.
  // Make property names self-documenting.
  // Remove properties that are only needed by the server and not the client.
  $return['vocabulary_id'] = $vocabulary[$v_vid]->vid;
  $return['vocabulary_name'] = $vocabulary[$v_vid]->name;
  $return['vocabulary_machine_name'] = $vocabulary[$v_vid]->machine_name;
  $return['vocabulary_description'] = $vocabulary[$v_vid]->description;
  $i = 0;
  foreach ($terms as $term) {
    $t = new stdClass();
    $t->term_name = $term->name;
    $t->term_description = $term->description;
    $t->term_weight = $term->weight;
    $t->term_uuid = $term->uuid;
    $t->term_synonyms = $term->synonyms;
    $t->term_parents = $term->parents;
    $t->term_relations = array();

    // We need to alter the array keys to assist our
    // custom formatter class TaxonomyServerServicesXMLFormatter.
    $return['terms']['term_' . $i++] = $t;
  }
  return $return;
}

/**
 * Returns all terms in a vocabulary.
 *
 * @param int $v_vid
 *   The term's vocabulary id.
 *
 * @return object
 *   An object.
 */
function taxonomy_server_get_all_terms($v_vid) {
  if (!$v_vid) {
    drupal_add_http_header('Status', '402 Unprocessable Entity');
    return array(
      'field' => 'vid',
      'message' => 'Parameter required',
      'errors' => array(
        'code' => 'missing_field',
        'datatype' => 'int',
        'format' => '',
      ),
    );
  }
  $synonyms_settings = variable_get('synonyms_settings_' . $v_vid, array());
  $synonyms_enabled = FALSE;
  if (module_exists('synonyms') && count($synonyms_settings) == 1) {
    $synonyms_enabled = TRUE;
  }
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->innerjoin('taxonomy_term_hierarchy', 'tth', 'tth.tid = ttd.tid');
  $query->innerjoin('taxonomy_vocabulary', 'tv', 'tv.vid = ttd.vid');
  $query->leftjoin('taxonomy_term_data', 'ttd_parent', 'tth.parent = ttd_parent.tid');

  if (module_exists('relation')) {
    // @TODO: this!
  }

  $query->addExpression('group_concat(ttd_parent.uuid)', 'parents');
  $query->fields('ttd')
    ->fields('tv', array('machine_name'))
    ->condition('ttd.vid', $v_vid)
    ->groupBy('ttd.tid')
    ->orderBy('ttd.tid');

  $results = $query->execute();
  if ($results) {
    $a = array();
    while ($row = $results->fetchObject()) {
      $term_query = new EntityFieldQuery();
      $term_query->entityCondition('entity_type', 'taxonomy_term');
      $term_query->entityCondition('bundle', $row->machine_name)
        ->entityCondition('entity_id', $row->tid);
      $term_result = $term_query->execute();
      if ($term_result) {
        $entity = entity_load('taxonomy_term', array($row->tid));
        $i = 0;
        unset($a);
        foreach ($entity[$row->tid]->parent_uuid as $key_p => $value_p) {
          // We need to alter the array keys to assist our
          // custom formatter class TaxonomyServerServicesXMLFormatter.
          $a['term_parents_parent_uuid_' . $i++] = $value_p;
        }
        $row->parents = $a;

        $i = 0;
        unset($a);
        foreach ($entity[$row->tid]->synonyms_synonyms as $s) {
          foreach ($s as $data) {
            // We need to alter the array keys to assist our
            // custom formatter class TaxonomyServerServicesXMLFormatter.
            $a['term_synonyms_name_' . $i++] = $data['value'];
          }
        }
        $row->synonyms = $a;
      }
      $return[] = $row;
    }
  }

  if (!count($return)) {
    // @TODO: Uncomment this out when testing is complete.
    // drupal_add_http_header('Status', '204 No Content');
  }
  else {
    drupal_add_http_header('Status', '200 OK');
  }
  return $return;
}

/**
 * Returns vocabulary information.
 *
 * @param int $v_vid
 *   The vocabulary id.
 *
 * @return object
 *   An object.
 */
function taxonomy_server_get_vocabulary($v_vid) {
  if (!$v_vid) {
    drupal_add_http_header('Status', '402 Unprocessable Entity');
    return array(
      'field' => 'vid',
      'message' => 'Parameter required',
      'errors' => array(
        'code' => 'missing_field',
        'datatype' => 'int',
        'format' => '',
      ),
    );
  }

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'taxonomy_vocabulary')
    ->propertyCondition('vid', $v_vid);

  $results = $query->execute();

  if (isset($results['taxonomy_vocabulary'])) {
    $tids = array_keys($results['taxonomy_vocabulary']);
    $return = entity_load('taxonomy_vocabulary', $tids);
  }
  return $return;
}

/**
 * Implements hook_rest_server_response_formatters_alter().
 *
 * Triggered when the REST server request a list of supported response formats.
 */
function taxonomy_server_rest_server_response_formatters_alter(&$formatters) {
  // This will change the formatter for xml, to use our custom one so it
  // renders correctly.
  $formatters['xml'] = array(
    'mime types' => array('application/xml', 'text/xml'),
    'formatter class' => 'TaxonomyServerServicesXMLFormatter',
  );
}

/**
 * Implements hook_rest_server_request_parsers_alter().
 *
 * Triggered when the REST server request a list of available request parsers.
 */
function taxonomy_server_rest_server_request_parsers_alter(&$parsers) {
  // Our content type is text/xml this changes the parser to be our custom one.
  $parsers['text/xml'] = "TaxonomyServerExampleXMLResponseParser::parse";
}

/**
 * Custom formatter class to handle XML formatting.
 */
class TaxonomyServerServicesXMLFormatter implements ServicesFormatterInterface {
  /**
   * Render function.
   */
  public function render($data) {
    $doc = new DOMDocument('1.0', 'utf-8');
    if (is_array($data->vocabulary)) {
      $root = $doc->createElement('vocabularies');
    }
    else {
      $root = $doc->createElement('vocabulary');
    }
    $doc->appendChild($root);

    $this->xml_recursive($doc, $root, $data);

    return $doc->saveXML();
  }

  /**
   * XML Recursive function.
   *
   * This function fails DCS because of the naming convention, but we are
   * implementing ServicesFormatterInterface which also contains this error.
   *
   * @param object $doc
   *   The XML document object.
   *
   * @param object $parent
   *   The parent XML document object.
   *
   * @param array $data
   *   The data to save.
   */
  private function xml_recursive(&$doc, &$parent, $data) {
    if (is_object($data)) {
      $data = get_object_vars($data);
    }

    if (is_array($data)) {
      foreach ($data as $key => $value) {
        if (is_numeric($key)) {
          $key = 'item';
        }
        elseif (strpos($key, 'term_') !== FALSE
          || strpos($key, 'vocabulary_') !== FALSE
          || strpos($key, 'term_parents_parent_uuid_') !== FALSE
          || strpos($key, 'term_synonyms_name_') !== FALSE) {
          $key = preg_replace('/_[0-9]+/', '', $key);
        }
        else {
          $key = preg_replace('/[^\w\d_]/', '_', $key);
          $key = preg_replace('/^(\d+)/', '_$1', $key);
        }
        $element = $doc->createElement($key);
        $parent->appendChild($element);
        $this->xml_recursive($doc, $element, $value);
      }
    }
    elseif ($data !== NULL) {
      $parent->appendChild($doc->createTextNode($data));
    }
  }
}

class TaxonomyServerExampleXMLResponseParser implements ServicesParserInterface {
  /**
   * Custom parser class to handle XML parsing.
   */
  public function parse(ServicesContextInterface $context) {
    // Get/hold the old error state.
    $old_error_state = libxml_use_internal_errors(TRUE);

    // Clear all libxml errors.
    libxml_clear_errors();

    // Get a new SimpleXmlElement object from the XML string.
    $xml_data = simplexml_load_string($context->getRequestBody());

    // If $xml_data is NULL then we expect errors.
    if (!$xml_data) {
      // Build an error message string.
      $message = '';
      $errors = libxml_get_errors();
      foreach ($errors as $error) {
        $message .= t('Line @line, Col @column: @message',
          array(
            '@line' => $error->line,
            '@column' => $error->column,
            '@message' => $error->message)
          ) . "\n\n";
      }

      // Clear all libxml errors and restore the old error state.
      libxml_clear_errors();
      libxml_use_internal_errors($old_error_state);

      // Throw an error - 406 / not acceptable response.
      services_error($message, 406);
    }
    // Whew, no errors, restore the old error state.
    libxml_use_internal_errors($old_error_state);

    // Unmarshal the SimpleXmlElement, and return the resulting array.
    $php_array = $this->unmarshalXML($xml_data, NULL);
    return (array) $php_array;
  }

  /**
   * A recusive function that unmarshals an XML string, into a php array.
   */
  protected function unmarshalXML($node, $array) {
    // For all child XML elements.
    foreach ($node->children() as $child) {
      if (count($child->children()) > 0) {
        // If the child has children.
        $att = 'is_array';
        if ($child->attributes()->$att) {
          $new_array = array();
          // Recursive through <item>.
          foreach ($child->children() as $item) {
            $new_array[] = $this->unmarshalXML($item, $array[$item->getName()]);
          }
        }
        else {
          // Else, simply create an array where the key is name of the element.
          $new_array = $this->unmarshalXML($child, $array[$child->getName()]);
        }
        // Add $new_array to $array.
        $array[$child->getName()] = $new_array;
      }
      else {
        // Use the is_raw attribute on value elements for select type fields to
        // pass form validation.
        if ($child->attributes()->is_raw) {
          return (string) $child;
        }
        $array[$child->getName()] = (string) $child;
      }
    }
    return $array;
  }
}
