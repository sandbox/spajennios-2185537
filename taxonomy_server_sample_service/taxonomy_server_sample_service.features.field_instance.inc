<?php
/**
 * @file
 * taxonomy_server_sample_service.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function taxonomy_server_sample_service_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-taxonomy_server_sample_vocabulary-synonyms_synonyms'
  $field_instances['taxonomy_term-taxonomy_server_sample_vocabulary-synonyms_synonyms'] = array(
    'bundle' => 'taxonomy_server_sample_vocabulary',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Please enter the synonyms which should be related to this term.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'synonyms_synonyms',
    'label' => 'Synonyms',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Please enter the synonyms which should be related to this term.');
  t('Synonyms');

  return $field_instances;
}
