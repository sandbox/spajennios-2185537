<?php
/**
 * @file
 * taxonomy_server_sample_service.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function taxonomy_server_sample_service_taxonomy_default_vocabularies() {
  return array(
    'taxonomy_server_sample_vocabulary' => array(
      'name' => 'Taxonomy Server Sample Vocabulary',
      'machine_name' => 'taxonomy_server_sample_vocabulary',
      'description' => 'Sample vocabulary to be used with the taxonomy_server_sample_vocabulary feature.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
