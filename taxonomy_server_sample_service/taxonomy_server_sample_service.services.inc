<?php
/**
 * @file
 * taxonomy_server_sample_service.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function taxonomy_server_sample_service_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'taxonomy_server_sample_service';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'taxonomy_server_sample_service';
  $endpoint->authentication = array();
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'taxonomy_server_vocabulary' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'taxonomy_term_by_uuid' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['taxonomy_server_sample_service'] = $endpoint;

  return $export;
}
